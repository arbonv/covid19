﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Covid19.Data
{
    /// <summary>
    /// class describing the information needed to display covid-19 information about the selected country
    /// The data is fetched from https://api.covid19api.com/total/country/[country]
    /// </summary>
    public class InfoCountry
    {
        public string Country { get; set; }

        public int Confirmed { get; set; }

        public int Deaths { get; set; }

        public int Active { get; set; }

        public int Recovered { get; set; }

        public DateTime DateTime { get; set; }
    }
}
