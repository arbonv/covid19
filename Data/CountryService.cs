﻿using Covid19.Pages;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Covid19.Data
{
    /// <summary>
    /// Service class needed in application.
    /// Responsible for fetching and deserializing data from api endpoints
    /// </summary>
    public class CountryService
    {

        public async Task<List<Country>> GetCountriesAsync(string url)
        {
            List<Country> countries = new List<Country>(); 
            using(HttpClient httpClient = new HttpClient())
            {
                string json = await httpClient.GetStringAsync(url);
                JArray array = JArray.Parse(json);
                foreach(JObject item in array)
                {
                    string name = item.GetValue("Country").ToString();
                    string slug = item.GetValue("Slug").ToString();
                    Country country = new Country(slug, name);
                    countries.Add(country);
                }
                return countries;
            }
        }

        public async Task<List<InfoCountry>> GetCountryStatisticAsync(string countryCode)
        {
            List<InfoCountry> infoCountryList = new List<InfoCountry>();
            var url = "https://api.covid19api.com/total/country/" + countryCode;
            using (HttpClient httpClient = new HttpClient())
            {
                string json = await httpClient.GetStringAsync(url);
                JArray array = JArray.Parse(json);

                foreach (JObject item in array)
                {
                    string country = item.GetValue("Country").ToString();
                    int confirmed = item.GetValue("Confirmed").ToObject<int>();
                    int deaths = item.GetValue("Deaths").ToObject<int>();
                    int recovered = item.GetValue("Recovered").ToObject<int>();
                    int active = item.GetValue("Active").ToObject<int>();
                    DateTime date = item.GetValue("Date").ToObject<DateTime>();
                    InfoCountry infoCountry = new InfoCountry
                    {
                        Country = country,
                        Confirmed = confirmed,
                        Active = active,
                        Deaths = deaths,
                        Recovered = recovered,
                        DateTime = date
                    };
                    infoCountryList.Add(infoCountry);
                }
                return infoCountryList;
            }
        }

    }
}
