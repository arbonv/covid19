﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Covid19.Data
{
    /// <summary>
    /// Class describing a country, with its name and "slugname"
    /// </summary>
    public class Country
    {

        public Country(string code, string name)
        {
            Slug = code;
            Name = name;
        }
        /// <summary>
        /// The slug or the "code" of the country used in covid19 api
        /// </summary>
        public string Slug { get; set; }
        /// <summary>
        /// The name of the class
        /// </summary>
        public string Name { get; set; }

    }
}
